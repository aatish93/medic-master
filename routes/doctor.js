var express = require('express');
var router = express.Router();
var multer = require('multer');
//var passport = require('passport');
//var LocalStrategy = require('passport-local').Strategy;
var upload = multer({dest:'.uploads'});
//var User=require('../models/doctor');
/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});

router.get('/addDoctor',ensureAuthenticated, function(req, res, next) {
  res.render('addDoctor', { title: 'Register Doctor' });
});

function ensureAuthenticated(req,res,next){
    if(req.isAuthenticated()){
      return next();
    }
    res.redirect('/users/login');
  }
module.exports = router;