var express = require('express');
var router = express.Router();
var multer = require('multer');
//var passport = require('passport');
//var LocalStrategy = require('passport-local').Strategy;
var upload = multer({dest:'.uploads'});
//var User=require('../models/patient');

router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});

router.get('/addPatient',ensureAuthenticated, function(req, res, next) {
  res.render('addPatient', { title: 'Register Patient' });
});

router.get('/viewPatient',ensureAuthenticated, function(req, res, next) {
  res.render('viewPatient', { title: 'View Patient' });
});

function ensureAuthenticated(req,res,next){
  if(req.isAuthenticated()){
    return next();
  }
  res.redirect('/users/login');
}
module.exports = router;